<?php

namespace App\Controller;

use App\Entity\Crud;
use App\Form\CrudType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;


class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repository = $doctrine->getRepository(Crud::class);
        $objet = $repository->findAll();
        return $this->render('main/index.html.twig', [
            'list' => $objet
        ]);
    }


    #[Route('/create', name: 'create')]
    public function  create(Request $request, ManagerRegistry $doctrine): Response
    {
        // just set up a fresh $task object (remove the example data)
        $crud = new Crud();
        $entityManager = $doctrine->getManager();
        $form = $this->createForm(CrudType::class, $crud);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$form->getData() //holds the submitted values
            // but, the original `$task` variable has also been updated
            $em = $form->getData();
            $entityManager->persist($em);
            $entityManager->flush();
            // ... perform some action, such as saving the task to the database
            $this->addFlash('notice', 'is submitted successfully');
            return $this->redirectToRoute('create');
        }

        return $this->renderForm('main/create.html.twig', [
            'form' => $form
        ]);
    }



    #[Route('/update/{id}', name: 'update')]
    public function update(ManagerRegistry $doctrine, $id, Request $request): Response
    {


        $entityManager = $doctrine->getManager();
        $crud = $entityManager->getRepository(Crud::class)->find($id);
        $form = $this->createForm(CrudType::class, $crud);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$form->getData() //holds the submitted values
            // but, the original `$task` variable has also been updated
            $em = $form->getData();
            $entityManager->persist($em);
            $entityManager->flush();
            // ... perform some action, such as saving the task to the database
            $this->addFlash('notice', 'Updated successfully');
            return $this->redirectToRoute('main');
        }
        return $this->renderForm('main/update.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    public function delete(EntityManagerInterface $entityManager, $id): Response
    {

        $crud = $entityManager->getRepository(Crud::class)->find($id);
        $entityManager->remove($crud);
        $entityManager->flush();
        $this->addFlash('notice', 'Is delete successfully');
        return $this->redirectToRoute('main');
    }
}
